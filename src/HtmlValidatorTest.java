import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import org.junit.Before;
import org.junit.Test;

public class HtmlValidatorTest {
    private HtmlValidator htmlValidator;
    private Queue<HtmlTag> htmlTagQueue;
    private HtmlTag htmlOpen;
    private HtmlTag bodyOpen;
    private HtmlTag htmlClose;
    private HtmlTag bodyClose;
    private HtmlTag img;
    private HtmlTag nul;
    private final String validHtmlFile = "valid_html.txt";
    private final String expectedValidHtmlOutput = "expected_valid_html_output.txt";
    private final String invalidHtmlFile = "invalid_html.txt";
    private final String expectedInvalidHtmlOutput = "expected_invalid_html_output.txt";

    /**
     * Creates a queue of HTML tags. The queue will contain two sets of tags.
     * One set is labeled "html" and the other is "body".
     */
    private void generateHtmlQueue() {
        htmlTagQueue = new LinkedList<HtmlTag>();
        htmlTagQueue.add(htmlOpen);
        htmlTagQueue.add(bodyOpen);
        htmlTagQueue.add(img);
        htmlTagQueue.add(bodyClose);
        htmlTagQueue.add(htmlClose);
    }

    /**
     * Return a queue of tokenized Html tags from a given input file.
     * 
     * @return a queue of Html Tags.
     */
    public Queue<HtmlTag> getHtmlTagsFromFile(String address)
            throws IOException {
        InputStream stream = new FileInputStream(address);
        StringBuffer buffer = new StringBuffer();

        while (true) {
            int ch = stream.read();
            if (ch < 0) {
                break;
            }
            buffer.append((char) ch);
        }
        stream.close();
        return HtmlTag.tokenize(buffer.toString());
    }

    @Before
    public void setUp() {
        htmlValidator = new HtmlValidator();
        htmlOpen = new HtmlTag("html");
        htmlClose = new HtmlTag("html", false);
        bodyOpen = new HtmlTag("body");
        bodyClose = new HtmlTag("body", false);
        img = new HtmlTag("img");
        nul = null;
    }

    /**
     * Test constructing and HtmlValidator object with an valid input queue.
     */
    @Test
    public void testHtmlValidatorQueueConstructor() {
        generateHtmlQueue();
        HtmlValidator validator = new HtmlValidator(htmlTagQueue);
        HtmlTag newTag = new HtmlTag("sample");

        validator.addTag(newTag);
        Queue<HtmlTag> validatorQueue = validator.getTags();

        Iterator<HtmlTag> validatorItr = validatorQueue.iterator();
        Iterator<HtmlTag> htmlQueueItr = htmlTagQueue.iterator();

        // Check that original elements were properly copied.
        while (validatorItr.hasNext() && htmlQueueItr.hasNext()) {
            assertTrue(htmlQueueItr.next().equals(validatorItr.next()));
        }

        // Check that a deep copy was made. Only the validator queue should
        // contain the new tag.
        if (htmlQueueItr.hasNext()) {
            fail("htmlQueue has unexpected amount of tags");
        }

        // Check tag was successfully added to the validator queue.
        if (validatorItr.hasNext()) {
            assertTrue(validatorItr.next().equals(newTag));
        } else {
            fail("New Html tag was not successfully added to validator queue");
        }

        // Check to make sure nothing else was added to the validator queue.
        if (validatorItr.hasNext()) {
            fail("Validator queue has unexpected amount of tags");
        }
    }

    /**
     * Test constructing and HtmlValidator object with a NULL input queue.
     */
    @Test
    public void testHtmlValidatorNullQueueConstructor() {
        try {
            HtmlValidator validator = new HtmlValidator(null);
            fail("IllegalArgumentException not thrown!");
        } catch (IllegalArgumentException e) {
            // expected exception
        }
    }

    /**
     * Test adding an Html Tag to the validator.
     */
    @Test
    public void testAddTag() {
        htmlValidator.addTag(bodyOpen);
        Queue<HtmlTag> validatorQueue = htmlValidator.getTags();

        assertEquals(bodyOpen.getElement(), validatorQueue.peek().getElement());
    }

    /**
     * Test adding an NULL Html Tag to the validator.
     */
    @Test
    public void testAddNullTag() {
        try {
            htmlValidator.addTag(nul);
            fail("IllegalArgumentException not thrown!");
        } catch (IllegalArgumentException e) {
            // expected exception
        }
    }

    /**
     * Test remove all elements in validator's queue that match an element.
     */
    @Test
    public void testRemoveAll() {
        // First Validate the the validator properly constructed the queue.
        generateHtmlQueue();
        HtmlValidator validator = new HtmlValidator(htmlTagQueue);

        Queue<HtmlTag> validatorQueue = validator.getTags();

        Iterator<HtmlTag> validatorItr = validatorQueue.iterator();
        Iterator<HtmlTag> htmlQueueItr = htmlTagQueue.iterator();

        while (validatorItr.hasNext() && htmlQueueItr.hasNext()) {
            assertEquals(htmlQueueItr.next().getElement(), validatorItr.next()
                    .getElement());
        }

        if (validatorItr.hasNext() || htmlQueueItr.hasNext()) {
            fail("Constructed HtmlValidator queue's length differs from input queue");
        }

        // Next remove all elements that match the input string and compare
        // again.

        validator.removeAll("body");
        validator.removeAll("img");
        validatorQueue = validator.getTags();

        for (HtmlTag tag : validatorQueue) {
            assertThat(tag.getElement(), is(not("body")));
            assertThat(tag.getElement(), is(not("img")));
        }
    }

    /**
     * Test remove all exception when input is null.
     */
    @Test
    public void testRemoveAllException() {
        try {
            htmlValidator.removeAll(null);
            fail("IllegalArgumentException not thrown!");
        } catch (IllegalArgumentException e) {
            // expected exception
        }
    }

    /**
     * Test Html validate method on balanced queue.
     */
    @Test
    public void testValidateValidHtmlTagQueue() {
        String testFileString = null;
        String expectedOutput = null;
        try {
            testFileString = ValidatorMain.readCompleteFileOrURL(validHtmlFile);
            expectedOutput = ValidatorMain
                    .readCompleteFileOrURL(expectedValidHtmlOutput);
        } catch (IOException e) {
            fail("IOException thrown");
        }

        Queue<HtmlTag> tags = HtmlTag.tokenize(testFileString);
        HtmlValidator validator = new HtmlValidator(tags);

        PrintStream stdoutOriginal = System.out;
        ByteArrayOutputStream testOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutput));
        validator.validate();
        System.setOut(stdoutOriginal);

        // Remove carriage return at the end of each line in the expected output
        // when checking. Without removing this, there are some discrepancies
        // between Windows and *NIX systems when comparing end of line
        // characters. The validate function will only print '\n' at the end of
        // the line regardless of the operating system.
        assertEquals(expectedOutput.trim().replaceAll("\r\n", "\n"), testOutput
                .toString().trim());
    }

    /**
     * Test Html validate method on unbalanced queue.
     */
    @Test
    public void testValidateNonValidHtmlTagQueue() {
        String testFileString = null;
        String expectedOutput = null;
        try {
            testFileString = ValidatorMain
                    .readCompleteFileOrURL(invalidHtmlFile);
            expectedOutput = ValidatorMain
                    .readCompleteFileOrURL(expectedInvalidHtmlOutput);
        } catch (IOException e) {
            fail("IOException thrown");
        }

        Queue<HtmlTag> tags = HtmlTag.tokenize(testFileString);
        HtmlValidator validator = new HtmlValidator(tags);

        PrintStream stdoutOriginal = System.out;
        ByteArrayOutputStream testOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutput));
        validator.validate();
        System.setOut(stdoutOriginal);

        // Remove carriage return at the end of each line in the expected output
        // when checking. Without removing this, there are some discrepancies
        // between Windows and *NIX systems when comparing end of line
        // characters. The validate function will only print '\n' at the end of
        // the line regardless of the operating system.
        assertEquals(expectedOutput.trim().replaceAll("\r\n", "\n"), testOutput
                .toString().trim());
    }

    /**
     * Test Html validate method on NULL queue.
     */
    @Test
    public void testValidateNullHtmlTagQueue() {
        String expectedOutput = "";

        PrintStream stdoutOriginal = System.out;
        ByteArrayOutputStream testOutput = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOutput));
        try {
            htmlValidator.validate();
        } catch (EmptyStackException e) {
            fail("EmptyStackException thrown!");
        }
        System.setOut(stdoutOriginal);

        // Ensure nothing was printed from call to validate method.
        assertEquals(expectedOutput, testOutput.toString());
    }

    /**
     * Test Html validate method on queue that causes an exception to be thrown.
     */
    @Test
    public void testValidateHtmlTagQueueExceptioin() {
        htmlValidator.addTag(htmlClose);
        try {
            htmlValidator.validate();
            fail("EmptyStackException not thrown!");
        } catch (EmptyStackException e) {
            // expected stack exception
        }
    }
}