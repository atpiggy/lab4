/*
 * Implementation of a simple stack for HtmlTags. You should implement this
 * class.
 */

import java.util.ArrayList;
import java.util.EmptyStackException;

public class MyStack {
    // An ArrayList to hold HtmlTag objects.
    // Use this to implement StackMP2.
    private ArrayList<HtmlTag> stack_internal;

    /**
     * Create an empty stack.
     */
    public MyStack() {
        this.stack_internal = new ArrayList<HtmlTag>();
    }

    /**
     * Push a tag onto the top of the stack.
     */
    public void push(HtmlTag tag) {
        stack_internal.add(0, tag);
    }

    /**
     * Removes the tag at the top of the stack. Should throw an exception if the
     * stack is empty.
     */
    public HtmlTag pop() {
        return stack_internal.remove(0);
    }

    /**
     * Looks at the object at the top of the stack but does not actually remove
     * the object. Should throw an exception if the stack is empty.
     * 
     * @return returns element at the top of the stack without removing it.
     * @throws EmptyStackException
     *             if the stack is empty.
     */
    public HtmlTag peek() throws EmptyStackException {
        if (stack_internal.isEmpty()) {
            throw new EmptyStackException();
        }
        return stack_internal.get(0);
    }

    /**
     * Tests if the stack is empty. Returns true if the stack is empty; false
     * otherwise.
     */
    public boolean isEmpty() {
        return (stack_internal.isEmpty());
    }
}
