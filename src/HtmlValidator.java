import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

/*
 * This is the HtmlValidator class. You should implement this class.
 */
public class HtmlValidator {
    private Queue<HtmlTag> validator;

    /**
     * Constructs an empty validator queue for HTML tags.
     */
    public HtmlValidator() {
        validator = new LinkedList<HtmlTag>();
    }

    /**
     * Constructs a validator queue and copies the tags from given queue into
     * the validator.
     * 
     * @param tags
     *            a queue containing HTML tags.
     */
    public HtmlValidator(Queue<HtmlTag> tags) {
        if (tags == null) {
            throw new IllegalArgumentException("Passed queue is null.");
        }

        validator = new LinkedList<HtmlTag>();
        for (HtmlTag tag : tags) {
            HtmlTag newTag = new HtmlTag(tag.getElement(), tag.isOpenTag());
            validator.add(newTag);
        }
    }

    /**
     * Add the given HTML tag to the end of the validator queue.
     * 
     * @param tag
     *            HTML tag to be added to the validator queue.
     * @throws IllegalArgumentException
     *             if the given tag is null.
     */
    public void addTag(HtmlTag tag) throws IllegalArgumentException {
        if (tag == null) {
            throw new IllegalArgumentException("The HTML tag passed is null.");
        }
        validator.add(tag);
    }

    /**
     * Return the validator's queue of HTML tags in its current state.
     * 
     * @return a queue containing the validator's HTML tags in its current
     *         state.
     */
    public Queue<HtmlTag> getTags() {
        return validator;
    }

    /**
     * Remove all elements in the validator's queue that matches the given
     * element.
     * 
     * @param element
     *            any element matching this string will be removed from the
     *            validator's queue.
     */
    public void removeAll(String element) {
        if (element == null) {
            throw new IllegalArgumentException();
        }

        Iterator<HtmlTag> itr = validator.iterator();
        while (itr.hasNext()) {
            if (element.equals(itr.next().getElement())) {
                itr.remove();
            }
        }
    }

    /**
     * Prints out indented text representation of the HTML tags in the
     * validator's queue. This will also validate that the tags are balanced.
     * (i.e. Each opening/closing tag is properly closed).
     * 
     * Error message regarding unbalanced tags will be printed to the standard
     * output.
     */
    public void validate() {
        MyStack validatorStack = new MyStack();
        String space = "    ";
        String indent = "";

        for (HtmlTag tag : validator) {
            // checks whether or not the tag is an open tag
            if (tag.isOpenTag()) {
                System.out.print(indent + tag.toString() + "\n");

                // if tag is not self closing, add it to the stack and add
                // indentation
                if (!tag.isSelfClosing()) {
                    validatorStack.push(tag);
                    indent += space;
                }
            } else {
                // check if the closing tag matches the top of the stack
                // and has a matching open tag
                boolean matches = false;
                try {
                    matches = tag.matches(validatorStack.peek());
                    if (matches) {
                        validatorStack.pop();
                        indent = indent.substring(0, indent.length() - 4);
                        System.out.print(indent + tag.toString() + "\n");
                    } else {
                        System.out.print("ERROR unexpected tag: "
                                + tag.toString() + "\n");
                    }
                } catch (IllegalArgumentException e) {
                    System.out.print("ERROR unexpected tag: "
                            + tag.toString() + "\n");
                }
            }
        }

        // The remaining tags do not have a matching close tag. Declare error.
        while (!validatorStack.isEmpty()) {
            HtmlTag tag = validatorStack.pop();
            System.out.print("ERROR unclosed tag: " + tag.toString() + "\n");
        }
    }
}
