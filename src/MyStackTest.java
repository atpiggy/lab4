import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Before;
import org.junit.Test;

public class MyStackTest {
    private MyStack testStack;
    private HtmlTag p;
    private HtmlTag html;
    private HtmlTag body;
    private HtmlTag nul;

    @Before
    public void setUp() {
        testStack = new MyStack();
        p = new HtmlTag("p");
        html = new HtmlTag("html");
        body = new HtmlTag("body", false);
        nul = null;
    }

    /**
     * Test pushing an element to the stack and peeking at top element on the
     * stack.
     */
    @Test
    public void testPushAndPeek() {
        testStack.push(p);
        assertEquals("<p>", testStack.peek().toString());
    }

    /**
     * Test push multiple items to the stack.
     */
    @Test
    public void testPushMultipleStack() {
        testStack.push(p);
        assertEquals("<p>", testStack.peek().toString());

        testStack.push(body);
        assertEquals("</body>", testStack.peek().toString());
    }

    /**
     * Test pushing a null object onto the stack.
     */
    @Test
    public void testPushNull() {
        testStack.push(nul);
        try {
            assertNull(testStack.peek().toString());
            fail("NullPointerException not thrown!");
        } catch (NullPointerException e) {
            System.out
                    .println("A null object in the stack throws a NullPointerException.");
        }
    }

    /**
     * Test the pop method.
     */
    @Test
    public void testPopStack() {
        testStack.push(html);
        assertEquals("<html>", testStack.peek().toString());
        assertEquals("<html>", testStack.pop().toString());
    }

    /**
     * Test empty stack after creating.
     */
    @Test
    public void testEmptyStackAfterCreating() {
        assertEquals(true, testStack.isEmpty());
    }

    /**
     * Test empty stack after pushing and popping an element.
     */
    @Test
    public void testEmptyStackPushingAndPopping() {
        testStack.push(html);
        assertEquals("<html>", testStack.peek().toString());
        assertEquals("<html>", testStack.pop().toString());
        assertEquals(true, testStack.isEmpty());
    }

    /**
     * Test EmptyStackException is thrown in peek method when stack is empty.
     */
    @Test
    public void testEmptyStackExceptionInPeek() {
        try {
            testStack.peek();
            fail("EmptyStackException not thrown!");
        } catch (EmptyStackException e) {
            System.out
                    .println("The EmptyStackException in MyStack.peek() is working correctly.");
        }
    }
}
